unit IdeaL.Lib.Facebook.Login;

interface

uses
  System.Classes,
  System.SysUtils,
  System.JSON,

  REST.Types,
  REST.Client,
  REST.Authenticator.OAuth,
  REST.Utils,

  Data.Bind.Components,
  Data.Bind.ObjectScope,

  FMX.WebBrowser,

  Web.HTTPApp;

type
  TProcAfterExcute = reference to procedure(var AUserId, ANome, ALastName,
    AEmail, AUrlPhoto, AToken: string);

  TFacebookLogin = class
  private
    { private declarations }
    FWebBrowser: TWebBrowser;
    FRESTRequestAfterExecute: TProcAfterExcute;
    FDidRESTRequestExecute: Boolean;
    FWebBrowserDidFinishLoad: TProc;
    FProcLogin: TProc;

    FAccessToken: string;

    FRESTRequest: TRESTRequest;
    FRESTClient: TRESTClient;
    FRESTResponse: TRESTResponse;
    FOAuth2Authenticator: TOAuth2Authenticator;

    procedure DoRESTRequestAfterExecute(Sender: TCustomRESTRequest);
    procedure DoWebBrowserDidFinishLoad(Sender: Tobject);

    function FacebookAccessTokenRedirect(const AUrl: string): Boolean;
    procedure SetWebBrowser(const Value: TWebBrowser);
  protected
    { protected declarations }
  public
    { public declarations }
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;

    property AccessToken: string read FAccessToken write FAccessToken;
    property OnLogin: TProc read FProcLogin write FProcLogin;
    property OnWebBrowserDidFinishLoad: TProc read FWebBrowserDidFinishLoad
      write FWebBrowserDidFinishLoad;
    property DidRESTRequestExecute: Boolean read FDidRESTRequestExecute;
    property WebBrowser: TWebBrowser read FWebBrowser write SetWebBrowser;
    property OnRESTRequestAfterExecute: TProcAfterExcute
      read FRESTRequestAfterExecute write FRESTRequestAfterExecute;

    function GetFacebookUserData: Boolean;
    procedure DoLogin(const AIdApp: string; const AAccessToken: string = '');
    procedure LogOut(const AAppId, AAccessToken: string);
  published
    { published declarations }
  end;

implementation

{ TFacebookLogin }

constructor TFacebookLogin.Create(AOwner: TComponent);
begin
  FRESTRequestAfterExecute := nil;
  FAccessToken := '';

  FDidRESTRequestExecute := False;

  FRESTRequest := TRESTRequest.Create(AOwner);
  FRESTClient := TRESTClient.Create(AOwner);
  FRESTResponse := TRESTResponse.Create(AOwner);
  FOAuth2Authenticator := TOAuth2Authenticator.Create(AOwner);

  { FRESTRequest.Accept := 'application/json, text/plain; q=0.9, text/html;q=0.8,';
    FRESTRequest.AcceptCharset := 'utf-8, *;q=0.8'; }
  FRESTRequest.Client := FRESTClient;
  FRESTRequest.Response := FRESTResponse;
  FRESTRequest.SynchronizedEvents := False;
  FRESTRequest.OnAfterExecute := DoRESTRequestAfterExecute;

  FRESTClient.Accept := 'application/json, text/plain; q=0.9, text/html;q=0.8,';
  FRESTClient.AcceptCharset := 'UTF-8, *;q=0.8';
  FRESTClient.Authenticator := FOAuth2Authenticator;
  FRESTClient.RaiseExceptionOn500 := False;

  FRESTResponse.ContentType := 'application/json';

  FOAuth2Authenticator.AuthorizationEndpoint :=
    'https://www.facebook.com/dialog/oauth';
end;

destructor TFacebookLogin.Destroy;
begin
  FreeAndNil(FOAuth2Authenticator);
  FreeAndNil(FRESTResponse);
  FreeAndNil(FRESTClient);
  FreeAndNil(FRESTRequest);
  inherited;
end;

procedure TFacebookLogin.DoLogin(const AIdApp, AAccessToken: string);
var
  LURL: string;
begin
  if(AAccessToken.Trim.IsEmpty)then
  begin
    LURL := 'https://www.facebook.com/dialog/oauth' + '?client_id=' +
      URIEncode(AIdApp) + '&response_type=token' + '&scope=' +
      URIEncode('public_profile,email')
    // + '&scope=' + URIEncode('user_about_me,user_birthday')
      + '&redirect_uri=' + URIEncode
      ('https://www.facebook.com/connect/login_success.html');

    // Abre tela de login do facebook...

    WebBrowser.Navigate(LURL);
    if (Assigned(FProcLogin)) then
      FProcLogin;
  end else
  begin
    FAccessToken := AAccessToken;
    GetFacebookUserData();
  end;
end;

procedure TFacebookLogin.DoRESTRequestAfterExecute(Sender: TCustomRESTRequest);
var
  LJsonObj: TJSONObject;
  LElements: TJSONValue;

  LJson: string;
  LUserId: string;
  LName: string;
  LLastName: string;
  LEmail: string;
  LUrlPhoto: string;
  LBirthday: string;
begin
  if (Assigned(FRESTRequestAfterExecute)) then
  begin
    try
      {para ter mais recursos, basta solicitar no Facebook Developer e ver como fica o JSON de retorno}
      FDidRESTRequestExecute := True;
      LJson := '';

      // Valida JSON retornado...
      if Assigned(FRESTResponse.JSONValue) then
        LJson := FRESTResponse.JSONValue.ToString;

      // Extrai campos do JSON...
      LJsonObj := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(LJson), 0)
        as TJSONObject;

      if (LJsonObj = nil) then
        raise Exception.Create
          ('Error on try get JsonObject from Facebook login result');

      try
        LUserId := HTMLDecode(StringReplace(TJSONObject(LJsonObj).Get('id')
          .JSONValue.ToString, '"', '', [rfReplaceAll]));
      except
      end;

      try
        LEmail := StringReplace(TJSONObject(LJsonObj).Get('email')
          .JSONValue.ToString, '"', '', [rfReplaceAll]);
      except
      end;

      try
        // Primeiro nome...
        LName := StringReplace(TJSONObject(LJsonObj).Get('first_name')
          .JSONValue.ToString, '"', '', [rfReplaceAll]);
      except
      end;

      try
        // Sobrenome...
        LLastName := StringReplace(TJSONObject(LJsonObj).Get('last_name')
          .JSONValue.ToString, '"', '', [rfReplaceAll]);
      except
      end;

      try
        LElements := TJSONObject(TJSONObject(LJsonObj).Get('picture').JSONValue)
          .Get('data').JSONValue;
        LUrlPhoto := StringReplace(TJSONObject(LElements).Get('url')
          .JSONValue.ToString, '"', '', [rfReplaceAll]);

        LUrlPhoto := StringReplace(LUrlPhoto, '\', '', [rfReplaceAll]);
      except
      end;
    finally
      FreeAndNil(LJsonObj);
    end;

    FRESTRequestAfterExecute(LUserId, LName, LLastName, LEmail, LUrlPhoto, FAccessToken);
  end;
  FAccessToken := '';
end;

function TFacebookLogin.FacebookAccessTokenRedirect(const AUrl: string)
  : Boolean;
var
  LATPos: integer;
  LToken: string;
begin
  Result := False;
  LATPos := Pos('access_token=', AUrl);

  if (LATPos > 0) then
  begin
    LToken := Copy(AUrl, LATPos + 13, Length(AUrl));

    if (Pos('&', LToken) > 0) then
    begin
      LToken := Copy(LToken, 1, Pos('&', LToken) - 1);
    end;

    FAccessToken := LToken;

    if (LToken <> '') then
    begin
      Result := True;
    end;
  end
  else
  begin
    LATPos := Pos('api_key=', AUrl);

    if LATPos <= 0 then
    begin
      LATPos := Pos('access_denied', AUrl);

      if (LATPos > 0) then
      begin
        // Acesso negado, cancelado ou usu�rio n�o permitiu o acesso...
        FAccessToken := '';
        Result := True;
      end;
    end;
  end;
end;

function TFacebookLogin.GetFacebookUserData: Boolean;
begin
  Result := False;
  if FAccessToken <> '' then
  begin
    FRESTRequest.ResetToDefaults;
    FRESTClient.ResetToDefaults;
    FRESTResponse.ResetToDefaults;

    FRESTClient.BaseURL := 'https://graph.facebook.com';
    FRESTClient.Authenticator := FOAuth2Authenticator;
    FRESTRequest.Resource :=
      'me?fields=first_name,last_name,email,picture.height(150)';
    FOAuth2Authenticator.AccessToken := FAccessToken;

    FRESTRequest.Execute;
  end;
end;

procedure TFacebookLogin.LogOut(const AAppId, AAccessToken: string);
var
  LUrl: string;
begin
  //LURL := 'http://facebook.com/logout.php?app_key=' + AAppId + '&session_key=' + AAccessToken;//&next={2}

  //LUrl := 'https://www.facebook.com/logout.php';
  //LUrl := 'https://www.facebook.com/logout.php?access_token=' + AAccessToken;
  //LUrl := 'https://www.facebook.com/logout.php?confirm=1&api_key=2139393642844516';
  //LUrl := 'https://graph.facebook.com/v2.6/me/messages?access_token=' + AAccessToken;
  //LUrl := 'https://m.facebook.com/logout.php?t=2506813406004811';
  //LUrl := 'http://m.facebook.com/logout.php?confirm=1&next=' + URIEncode('https://www.facebook.com/connect/login_success.html');
  //LUrl := 'https://www.facebook.com/logout.php?next=' + URIEncode('https://www.facebook.com/connect/login_success.html') + '&access_token=' + AAccessToken;
  //LUrl := 'https://m.facebook.com/logout.php?h=AfdOx0N_lIpf7zoL&t=369619076992174'; //Pessoal
  LUrl := 'https://m.facebook.com/logout.php?h=AfdTCkWlAnuP_-A_&t=1558663279'; //IdeaL //&source=mtouch_logout_button&persist_locale=1&button_name=logout&button_location=settings
  //LUrl := 'https://m.facebook.com/logout.php?h=AfdTCkWlAnuP_-A_';
  {LUrl := 'Var session = ' + QuotedStr('<%= GET[' + QuotedStr('token') + '] %>')
    + ';' + 'var newURL = https://www.facebook.com/logout.php?next=' +
    URIEncode('https://www.facebook.com/connect/login_success.html') +
    '&access_token=+ session ' + 'window.location = newURL;';}

  {LURL := 'https://www.facebook.com/dialog/oauth' + '?client_id=' +
    URIEncode(AIdApp) + '&response_type=token' + '&scope=' +
    URIEncode('public_profile,email')
  // + '&scope=' + URIEncode('user_about_me,user_birthday')
    + '&redirect_uri=' + URIEncode
    ('https://www.facebook.com/connect/login_success.html');}

  // Abre tela de login do facebook...
  {WebBrowser.URL := LURL;
  WebBrowser.Navigate(LUrl);
  if (Assigned(FProcLogin)) then
    FProcLogin;}

  LUrl := 'javascript:(function(){var d = new Date();var name="c_user";var '+
  'domain=".facebook.com";var path="/";var expires = ";expires="+d;document.'+
  'cookie = name + "=" +( ( path ) ? ";path=" + path : "") +( ( domain ) ? '+
  '";domain=" + domain : "" ) +";expires="+expires;location.reload();})();';
  (*LUrl :=
  '<script type="text/javascript">' +
  'function logoutFacebook()' +
  '{' +
  'var d = new Date();' +
  'var name="c_user";' +
  'var domain=".facebook.com";' +
  'var path="/";' +
  'var expires = ";expires="+d;' +
  'document.cookie = name + "=" +( ( path ) ? ";path=" + path : "") +( ( domain ) ? ";domain=" + domain : "" ) +";expires="+expires;' +
  'location.reload();' +
  '}' +
  'logoutFacebook();' +
  '</script>' ;*)

  (*LUrl :=
  'function logoutFacebook()' +
  '{' +
  'var d = new Date();' +
  'var name="c_user";' +
  'var domain=".facebook.com";' +
  'var path="/";' +
  'var expires = ";expires="+d;' +
  'document.cookie = name + "=" +( ( path ) ? ";path=" + path : "") +( ( domain ) ? ";domain=" + domain : "" ) +";expires="+expires;' +
  'location.reload();' +
  '}';*)

  WebBrowser.EvaluateJavaScript(LUrl);
end;

procedure TFacebookLogin.SetWebBrowser(const Value: TWebBrowser);
begin
  FWebBrowser := Value;
  FWebBrowser.OnDidFinishLoad := DoWebBrowserDidFinishLoad;
end;

procedure TFacebookLogin.DoWebBrowserDidFinishLoad(Sender: Tobject);
var
  LCloseWebView: Boolean;
  LURL: string;
begin
  inherited;
  LURL := WebBrowser.URL;
  LCloseWebView := False;

  if (LURL <> '')and not(FDidRESTRequestExecute) then
    LCloseWebView := FacebookAccessTokenRedirect(LURL);

  if (LCloseWebView) and not(FDidRESTRequestExecute) then
  begin
    WebBrowser.Stop;
    WebBrowser.URL := '';
    GetFacebookUserData;

    if (Assigned(FWebBrowserDidFinishLoad)) then
      FWebBrowserDidFinishLoad;
  end;
end;

end.
