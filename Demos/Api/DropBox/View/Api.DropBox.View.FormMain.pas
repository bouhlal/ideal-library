unit Api.DropBox.View.FormMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.StdCtrls, FMX.Controls.Presentation,

  // Necessary uses
  System.Math, // to be used on DownloadUploadStatusUpdate  and validate the division by zero
  System.Net.HttpClient, // To create the CallBack procedure DownloadUploadRequestCompletedEvent
  System.Permissions //
  ;

type
  TFormMain = class(TForm)
    Label1: TLabel;
    btnUpload: TButton;
    btnDownload: TButton;
    ProgressBar1: TProgressBar;
    btnDelete: TButton;
    btnListFolder: TButton;
    btnCreateFolder: TButton;
    procedure btnUploadClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    // Your private DropBox Developer AccessToken
    const CAccessToken = 'z5bcG43gKeAAAAAAAAAAZFRKWz0UH25B729PDv0b5RkcogoBJMGHDQBOcJO3Bc_Q';
      CPngFeleName = 'DemoApiDropbox.png';
    function GetLocalFilePath: string;
    procedure DisplayRationale(Sender: TObject; const APermissions: TArray<string>; const APostRationaleProc: TProc);
    procedure GrantedPermissionRequestResult(Sender: TObject; const APermissions: TArray<string>; const AGrantResults: TArray<TPermissionStatus>);
    procedure DownloadUploadStatusUpdate(const ALength, ACount: Int64);
    procedure DownloadUploadRequestCompletedEvent(const Sender: TObject; const AResponse: IHTTPResponse);

    procedure EverythingIsHere;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses
  IdeaL.Lib.Api.DropBox,
  IdeaL.Lib.Utils,

  {
  Uses to request permission, PLEASE do that in your own way
  }
{$IFDEF ANDROID}
  Androidapi.Helpers,
  Androidapi.JNI.JavaTypes,
  Androidapi.JNI.Os,
{$ENDIF}
  FMX.DialogService,
  System.Generics.Collections;
{$R *.fmx}

{ TFormMain }

procedure TFormMain.btnUploadClick(Sender: TObject);
var
  LPathLocal: string;
  LPathRemote: string;
  LDb: TDropBoxApi;
  LJson: string;
begin
  LPathLocal := GetLocalFilePath;
  LPathLocal := TUtils.Combine(LPathLocal, [CPngFeleName]);
  LPathRemote := '/' + CPngFeleName;
  LDb := TDropBoxApi.Create;
  try
    LDb.AccessToken := CAccessToken;
    {LDb.OnDownloadStatusUpdate := DownloadUploadStatusUpdate;
    LDb.OnRequestCompletedEvent := DownloadUploadRequestCompletedEvent;}

    if Sender = btnUpload then
    begin
      if not(FileExists(LPathLocal)) then
        raise Exception.Create('File doesnt exist');

      LDb.Upload(LPathLocal, LPathRemote)
    end
    else if Sender = btnDownload then
      LDb.Download(LPathLocal, LPathRemote)
    else if Sender = btnDelete then
    begin
      {
      You can delete files or paths;

      If the file doesn't exists or can't be deleted, it will return Conflict
      }

      LPathRemote := '/' + 'FolderHasBeenCreated';
      LDb.Delete(LPathRemote);
    end
    else if Sender = btnListFolder then
    begin
      {
      There is many params, look on the documentation to understand why they are for;

      The result is a JSON, so good luck
      }
      LPathRemote := '';
      LJson := LDb.ListFolder(LPathRemote);
    end
    else if Sender = btnCreateFolder then
    begin
      {
      If the path already exists, it will return Conflict
      }

      LPathRemote := '/' + 'FolderHasBeenCreated';
      LDb.CreateFolder(LPathRemote)
    end
    ;
  finally
    FreeAndNil(LDb);
  end;
end;

procedure TFormMain.Button1Click(Sender: TObject);
var
  LReadExternalStorage: string;
  LWriteExternalStorage: string;
begin
{$IFDEF ANDROID}
  LReadExternalStorage := JStringToString(TJManifest_permission.JavaClass.READ_EXTERNAL_STORAGE);
  LWriteExternalStorage := JStringToString(TJManifest_permission.JavaClass.WRITE_EXTERNAL_STORAGE);
{$ENDIF}
  PermissionsService.RequestPermissions([
  LReadExternalStorage,
  LWriteExternalStorage
  ], GrantedPermissionRequestResult, DisplayRationale);
end;

procedure TFormMain.DisplayRationale(Sender: TObject;
  const APermissions: TArray<string>; const APostRationaleProc: TProc);
begin
  // Show an explanation to the user *asynchronously* - don't block this thread waiting for the user's response!
  // After the user sees the explanation, invoke the post-rationale routine to request the permissions
  TDialogService.ShowMessage('Your Message Here',
    procedure(const AResult: TModalResult)
    begin
      APostRationaleProc;
    end);
end;

procedure TFormMain.DownloadUploadRequestCompletedEvent(const Sender: TObject;
  const AResponse: IHTTPResponse);
begin
  {
  Do whatever you want.
  You can do something here or you can just wait for the exception, please, make sure to use TryExcept
  }
  if AResponse.StatusCode <> 200 then
    ShowMessage(AResponse.StatusText)
  else
    ShowMessage('Well done ;)');
end;

procedure TFormMain.DownloadUploadStatusUpdate(const ALength, ACount: Int64);
begin
  {
  Unfortunately, Upload has no Lenght, to fix it, you can do it manually, do your way to get the file length use it here
  }
  ProgressBar1.Value := (ACount / System.Math.IfThen(ALength = 0, 1, ALength)) * 100;
end;

procedure TFormMain.EverythingIsHere;
begin

end;

function TFormMain.GetLocalFilePath: string;
begin
  {
  Doesn't matter how you'll do that, it's not my problem
  }
  Result := TUtils.GetApplicationPath;
{$IFDEF MSWINDOWS}
  Result := TUtils.RemovePathFromDir(Result, 3);
  Result := TUtils.Combine(Result, ['File']);
{$ENDIF}
end;

procedure TFormMain.GrantedPermissionRequestResult(Sender: TObject;
  const APermissions: TArray<string>;
  const AGrantResults: TArray<TPermissionStatus>);
begin
  btnUploadClick(btnDownload);
end;

end.
