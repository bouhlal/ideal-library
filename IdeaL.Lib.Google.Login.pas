unit IdeaL.Lib.Google.Login;

interface

uses
  System.Classes,
  System.SysUtils,
  System.JSON,

  REST.Utils,

  FMX.WebBrowser, REST.Client, REST.Authenticator.OAuth, REST.Types;

type
  TGoogleLogin = class
  private
    { private declarations }
    FWebBrowser: TWebBrowser;
    FIdClient: string;
    FAuthCode: string;
    FAccessToken: string;
    FRefreshToken: string;
    FId: string;
    FEmail: string;
    FPersonsName: string;
    FFirstPersonsName: string;
    FLinkProfile: string;
    FUrlPhoto: string;

    procedure SetWebBrowser(const Value: TWebBrowser);

    procedure OnDidFinishLoadGoogle(ASender: TObject);
    procedure OnShouldStartLoadWithRequest(ASender: TObject; const URL: string);
  protected
    { protected declarations }
  public
    { public declarations }
    constructor Create(AOwner: TComponent);

    property Id: string read FId write FId;
    property Email: string read FEmail write FEmail;
    property PersonsName: string read FPersonsName write FPersonsName;
    property FirstPersonsName: string read FFirstPersonsName write FFirstPersonsName;
    property LinkProfile: string read FLinkProfile write FLinkProfile;
    property UrlPhoto: string read FUrlPhoto write FUrlPhoto;

    property WebBrowser: TWebBrowser read FWebBrowser write SetWebBrowser;
    property IdClient: string read FIdClient write FIdClient;
    property AuthCode: string read FAuthCode write FAuthCode;
    property AccessToken: string read FAccessToken write FAccessToken;
    property RefreshToken: string read FRefreshToken write FRefreshToken;

    procedure DoLogin(const AIdClient: string; const AAuthCode: string = '');
    function GetInfoGoogle(): Boolean;
  published
    { published declarations }
  end;

implementation

uses
  IdGlobal, IdURI;

{ TGoogleLogin }

constructor TGoogleLogin.Create(AOwner: TComponent);
begin

end;

procedure TGoogleLogin.DoLogin(const AIdClient, AAuthCode: string);
Var
  URL: string;
  Uri: String;
  FScope: string;
begin
  FAuthCode := AAuthCode;
  if (FAuthCode.Trim.IsEmpty) then
  Begin

    if FScope = '' then
      FScope := 'openid';

{$IFDEF MSWINDOWS}
    Uri := 'http://google.com';
{$ELSE}
    Uri := 'urn:ietf:wg:oauth:2.0:oob';
{$ENDIF}
    Uri := 'urn:ietf:wg:oauth:2.0:oob';
    URL := 'https://accounts.google.com/o/oauth2/v2/auth?' + 'scope=' +
      URIEncode(FScope) + '&access_type=' + URIEncode('offline') +
      '&include_granted_scopes=' + URIEncode('true') + '&state=' +
      URIEncode('state_parameter_passthrough_value') + '&redirect_uri=' +
      URIEncode(Uri) + '&response_type=' + URIEncode('code') + '&client_id=' +
      URIEncode(AIdClient);

    { if FLoginHint <> '' then
      URL := URL + '&login_hint=' + URIEncode(FLoginHint); }


    WebBrowser.OnShouldStartLoadWithRequest := OnShouldStartLoadWithRequest;
    WebBrowser.OnDidFinishLoad := OnDidFinishLoadGoogle;
    // WebBrowser.SetUserAgent('Mozilla/5.0 Google');

    { if FMessageAuth <> '' then
      frmAuthGoogle.SetMsg(FMessageAuth); }
    WebBrowser.Navigate(URL);
  End;
end;

function TGoogleLogin.GetInfoGoogle: Boolean;
Var
  LClient: TRESTClient;
  LRequest, LRequestFr: TRESTRequest;
  LResponse: TRESTResponse;
  LOAuth2: TOAuth2Authenticator;
  JSONRet: string;
  FScope: string;
begin
  Result := False;
  LOAuth2 := nil;
  LResponse := nil;

  try
    LResponse := TRESTResponse.Create(nil);

    LClient := TRESTClient.Create('https://www.googleapis.com');
    //LClient.BaseURL := 'https://accounts.google.com/';

    LRequest := TRESTRequest.Create(nil);
    LRequest.Client := LClient;

    if FScope = '' then
      FScope :=
      //'https://www.googleapis.com/oauth2/v4/token'
      //'https://www.googleapis.com/auth2/openid'
      //'https://www.googleapis.com/auth/drive.metadata.readonly'
      // 'https://www.googleapis.com/auth/plus.login'
       'https://www.googleapis.com/auth/userinfo.email'
        ;

    if (FAccessToken = '') Or (FRefreshToken = '') then
    Begin
      {LRequest.Client := LClient;                       // chain the request to the client
      LRequest.Method := rmPOST;
      LRequest.Resource := ExRequestResource;
      // required parameters
      LRequest.AddParameter(ExCode, AuthCode, pkGETorPOST);
      LRequest.AddParameter(ExClientID, ClientID, pkGETorPOST);
      LRequest.AddParameter(ExClientSecret, ClientSecret, pkGETorPOST);
      LRequest.AddParameter(ExRedirectURI, RedirectionEndPoint, pkGETorPOST);
      LRequest.AddParameter(ExGrantType, ExAuthorizationCode, pkGETorPOST);

      LRequest.Execute;}
      //https://developers.google.com/identity/protocols/OAuth2InstalledApp#step1-code-verifier
      LRequest.Method := TRESTRequestMethod.rmPOST;
      LRequest.Resource := 'oauth2/v4/token';
      LRequest.AddParameter('code', FAuthCode, TRESTRequestParameterKind.pkGETorPOST);
      LRequest.AddParameter('client_id', IdClient, TRESTRequestParameterKind.pkGETorPOST);
      LRequest.AddParameter('client_secret', 'iCtetsZThyQNaA7Gq09B2bxW', TRESTRequestParameterKind.pkGETorPOST);
      LRequest.AddParameter('redirect_uri', 'https://oauth2.example.com/code'{'urn:ietf:wg:oauth:2.0:oob'}, TRESTRequestParameterKind.pkGETorPOST);
      LRequest.AddParameter('grant_type', 'authorization_code', TRESTRequestParameterKind.pkGETorPOST);
      LRequest.Execute;

      LRequest.Response.GetSimpleValue('access_token', FAccessToken);
      LRequest.Response.GetSimpleValue('refresh_token', FRefreshToken);
    End;

    if FAccessToken <> '' then
    Begin
      LClient.ResetToDefaults;
      LRequest.ResetToDefaults;

      LRequest.Client := LClient;

      LOAuth2 := TOAuth2Authenticator.Create(nil);
      LOAuth2.AccessTokenEndpoint :=
        'https://accounts.google.com/o/oauth2/token';
      LOAuth2.AuthorizationEndpoint :=
        'https://accounts.google.com/o/oauth2/auth';
      LOAuth2.Scope := FScope;
      LOAuth2.RedirectionEndpoint := 'urn:ietf:wg:oauth:2.0:oob';
      LOAuth2.AccessToken := FAccessToken;
      LOAuth2.RefreshToken := FRefreshToken;
      LOAuth2.AccessTokenParamName := 'access_token';
      LOAuth2.ResponseType := TOAuth2ResponseType.rtTOKEN;

      LClient.BaseURL := 'https://www.googleapis.com/oauth2/v1/';
      LClient.Authenticator := LOAuth2;

      LRequest.Resource := 'userinfo?alt=json';
      LRequest.Execute;

      JSONRet := LRequest.Response.Content;

      LRequest.Response.GetSimpleValue('id', FId);
      LRequest.Response.GetSimpleValue('email', FEmail);
      LRequest.Response.GetSimpleValue('name', FPersonsName);
      LRequest.Response.GetSimpleValue('given_name', FFirstPersonsName);
      LRequest.Response.GetSimpleValue('link', FLinkProfile);
      LRequest.Response.GetSimpleValue('picture', FUrlPhoto);

      Result := True;
    End;
  Finally
    FreeAndNil(LOAuth2);
    FreeAndNil(LRequest);
    FreeAndNil(LClient);
    FreeAndNil(LResponse);
  End;
end;

procedure TGoogleLogin.OnDidFinishLoadGoogle(ASender: TObject);
Var
  PosI, PosF: Integer;
  js, URL: String;
begin
  URL := TWebBrowser(ASender).URL;

  {if (FAuthCode = '') And (POS('accounts', URL) > 0) And
    (POS('srfsign', URL) > 0) then}
  if (FAuthCode = '') and (Pos('state=state_parameter_passthrough_value', URL) > 0)
  and (Pos('code=4', URL) > 0) then
  Begin
    { TWebBrowser(ASender).Align := TAlignLayout.None;
      TWebBrowser(ASender).Height := 1; }
    js := 'var markup = document.documentElement.innerHTML;' + #13 + #10 +
      'var newURL = "http://1.1.1.1/" + markup;' + #13 + #10 +
      'window.location = newURL;';
    TWebBrowser(ASender).EvaluateJavaScript(js);
  End;

  if POS('https://www.google.com.br/?1=1', URL) > 0 then
  Begin
    WebBrowser.Stop;

  End;
end;

procedure TGoogleLogin.OnShouldStartLoadWithRequest(ASender: TObject;
  const URL: string);
Var
  jsonStr, ret: String;
  PosI, PosF: Integer;
begin
  if FAuthCode = '' then
    if (POS('http://1.1.1.1/', URL) = 1) then
    begin
      jsonStr := URL;
      Fetch(jsonStr, 'http://1.1.1.1/');
      jsonStr := TIdURI.URLDecode(jsonStr, IndyTextEncoding_UTF8);
      PosI := POS('<title>', jsonStr);
      if PosI > 0 then
        ret := Copy(jsonStr, PosI + 7, 250);

      PosF := POS('</title>', ret);
      if PosF > 0 then
        ret := Copy(ret, 1, PosF - 1);

      PosI := POS('=', ret);
      if PosI > 0 then
        ret := Copy(ret, PosI + 1, 200);

      if ret <> '' then
      Begin
        FAuthCode := ret;
        { if FAutoSave then
          CreateIniFile('Google', 'AuthCode', FAuthCode);

          TWebBrowser(ASender).Align := TAlignLayout.None;
          TWebBrowser(ASender).Height := 1;
          finishedProcess := True;

          frmAuthGoogle.ModalResult := mrOk; }
        TWebBrowser(ASender).URL := 'https://www.google.com.br/?1=1'
      End;
    end;
end;

procedure TGoogleLogin.SetWebBrowser(const Value: TWebBrowser);
begin
  FWebBrowser := Value;
end;

end.
